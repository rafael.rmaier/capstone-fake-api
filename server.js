const jsonServer = require("json-server");
const auth = require("json-server-auth");

const app = jsonServer.create();
const router = jsonServer.router("db.json");
const port = process.env.PORT || 3001;
const cors = require("cors");
app.use(cors());
app.db = router.db;

const rules = auth.rewriter({
  "/users*": "/660/users$1",
  "/habUnits*": "/660/habunits$1",
  "/products*": "/660/products$1",
  "/services*": "/660/services$1",
  "/guests*": "/660/guests$1",
  "insomnia.json": "/insomnia.json",
});

app.use(rules);
app.use(auth);
app.use(router);
app.listen(port);

console.log("Server is running on port:", port);
